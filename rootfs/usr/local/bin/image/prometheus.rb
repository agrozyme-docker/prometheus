# require_relative('../docker_core')
require('docker_core')

module DockerCore
  module Image
    module Prometheus
      module Build
        def self.main
          etc = '/etc/prometheus'
          System.run('apk add --no-cache prometheus')
          FileUtils.mv("#{etc}/.prometheus.yml", "#{etc}/prometheus.yml")
          FileUtils.mv("#{etc}/consoles/index.html.example", "#{etc}/consoles/index.html")
          Shell.make_folders('/prometheus', '/usr/local/etc/prometheus')
        end
      end

      module Run
        def self.main
          Shell.update_user
          # Shell.change_owner(html)
          System.execute('/usr/bin/prometheus', { 'config.file': '/etc/prometheus/prometheus.yml', 'storage.tsdb.path': '/prometheus', 'web.console.libraries': '/etc/prometheus/console_libraries', 'web.console.templates': '/etc/prometheus/consoles' }, '')
        end

      end
    end
  end
end
