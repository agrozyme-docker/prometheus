# Summary

Source: https://gitlab.com/agrozyme-docker/prometheus

Prometheus is a systems and service monitoring system.

# Settings

- Port: `9090`
- Storage Path: `/prometheus`
- Default Configuration: `/etc/prometheus/prometheus.yml`
- Custom Configuration Files: `/usr/local/etc/prometheus/*.yml`
