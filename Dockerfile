ARG DOCKER_REGISTRY=docker.io
ARG DOCKER_NAMESPACE=agrozyme
FROM ${DOCKER_REGISTRY}/${DOCKER_NAMESPACE}/alpine
COPY rootfs /
RUN chmod +x /usr/local/bin/* \
  && gem update -N docker_core \
  && gem clean \
  && /usr/local/bin/docker_build.rb
EXPOSE 9090
CMD ["/usr/local/bin/docker_run.rb"]
